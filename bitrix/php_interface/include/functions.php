<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 03.01.19
 * Time: 17:18
 */

function test_dump($var, $die = false, $all = false)
{
    global $USER;
    if( ($USER->GetID() == 1) || ($all == true))
    {
        ?>
        <font style="text-align: left; font-size: 10px"><pre><?var_dump($var)?></pre></font><br>
        <?
    }
    if($die)
    {
        die;
    }
}

/*
function ChekPageParamAndShowH1($pageProp)
{
    global $APPLICATION;
    if ($APPLICATION->getPageProperty($pageProp) !== "Y") {
        return '<div class="main_title"><h1>' . $APPLICATION->getTitle() . '</h1></div>';
    }
}

function ShowH1($pageProp)
{
    global $APPLICATION;
    $APPLICATION->AddBufferContent('CheckPageParamAndShowH1', $pageProp);
}
*/