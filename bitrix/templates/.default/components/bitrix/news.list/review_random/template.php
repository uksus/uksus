<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<!--    --><?// test_dump($arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"]); ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="sb_reviewed">
        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="sb_rw_avatar" alt=""/>
        <span class="sb_rw_name"><?echo $arItem["NAME"]?></span>
        <span class="sb_rw_job"><? echo $arItem['PROPERTIES']["POSITION"]["VALUE"]?> “<? echo $arItem['PROPERTIES']["COMPANY"]["VALUE"]?>”</span>
        <p>“<? echo $arItem["DETAIL_TEXT"]?>”</p>
        <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="sl_more">Подробнее &rarr;</a>
        <div class="clearboth"></div>
        <div class="sb_rw_arrow"></div>
    </div>
<?endforeach;?>
</div>
