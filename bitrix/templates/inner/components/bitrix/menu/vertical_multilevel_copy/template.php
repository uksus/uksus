<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//    test_dump($arResult);
?>

<?if (!empty($arResult)):?>
<!--<ul id="vertical-multilevel-menu">-->
<div class="sb_nav"><ul> <!-- Begin -->

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>
<!--    Есть дети-->
		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
<!--        Первый уровень вложенности-->
            <li class="<? if ($arItem["SELECTED"]): ?>close<? else: ?>open current<? endif; ?>">

                <span class="sb_showchild"></span>
				<!--<div class="root-item">-->
                <a href="<?=$arItem["LINK"]?>">
                    <span>
                        <?=$arItem["TEXT"]?>
                    </span>
                </a>

		<?else:?>
<!--        Не первый уровень вложенности-->
			<><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				<ul>
		<?endif?>

	<?else:?>
<!--    Нет детей-->
		<?if ($arItem["PERMISSION"] > "D"):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
<!--            Первый уровень вложенности-->

                <li class="close">
                    <a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
                </li>

			<?else:?>
<!--            Не первый уровень вложенности-->
            <ul>
                <li>
                    <a href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></></a>
                </li>
            </ul >
			<?endif?>
		<?else:?>
<!--            Ниче не показываем нет доступа-->
		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul >
</div>

<?endif?>